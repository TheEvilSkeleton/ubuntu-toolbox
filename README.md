# Ubuntu Toolbox

Ubuntu Toolbox image. This image is meant to be used with the `toolbox` command.

## Installation

```
toolbox create -i registry.gitlab.com/theevilskeleton/ubuntu-toolbox
```

## Remove

```
toolbox rmi -f ubuntu-toolbox
```

