#        Copyright (C) 2021  TheEvilSkeleton
#
#        This program is free software: you can redistribute it and/or modify
#        it under the terms of the GNU Affero General Public License as
#        published by the Free Software Foundation, either version 3 of the
#        License, or (at your option) any later version.
#
#        This program is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU Affero General Public License for more details.
#
#        You should have received a copy of the GNU Affero General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

FROM docker.io/library/ubuntu:latest

ENV NAME=ubuntu VERSION=latest
LABEL com.github.containers.toolbox="true" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Ubuntu toolbox containers"

RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install \
    bash-completion \
    git \
    keyutils \
    libcap2-bin \
    lsof \
    man-db \
    mlocate \
    rsync \
    sudo \
    tcpdump \
    time \
    traceroute \
    tree \
    unzip \
    wget \
    zip
RUN apt-get clean

# Enable sudo permission for wheel users
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/toolbox

RUN echo VARIANT_ID=container >> /etc/os-release
RUN touch /etc/localtime

CMD /bin/sh

